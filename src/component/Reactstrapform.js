
import { TextareaAutosize } from "@mui/material"
import { Button, Col, Container, FormLabel, Row } from "react-bootstrap"
import { Input } from "reactstrap"

function Reactstrapform() {
    return (
        <Container className="container-fluid" style={{backgroundColor:"olive"}}>
            <Row className="text-center mt-3">
                <h3>REGISTRATION FORM</h3>
            </Row>
            <Row className="text-center">
                <Col sm={6}>
                    <Row className="mt-3">
                        <Col sm={3}>
                            <FormLabel>First Name <span className="text-danger">(*)</span></FormLabel>
                        </Col>
                        <Col sm={9}>
                            <Input></Input>
                        </Col>
                    </Row>
                    <Row className="mt-3">
                        <Col sm={3}>
                            <FormLabel>Last Name <span className="text-danger">(*)</span></FormLabel>
                        </Col>
                        <Col sm={9}>
                            <Input></Input>
                        </Col>
                    </Row>
                    <Row className="mt-3">
                        <Col sm={3}>
                            <FormLabel>Birth Day <span className="text-danger">(*)</span></FormLabel>
                        </Col>
                        <Col sm={9}>
                            <Input></Input>
                        </Col>
                    </Row>
                    <Row className="mt-3">
                        <Col sm={3}>
                            <FormLabel>Gender <span className="text-danger">(*)</span></FormLabel>
                        </Col>
                        <Col sm={9}>
                            <Row>
                                <Col sm={6}>
                                    <Input type="radio" name="gender" />
                                    <FormLabel className="mx-2">Male</FormLabel>
                                </Col>
                                <Col sm={6}>
                                    <Input type="radio" name="gender" />
                                    <FormLabel className="mx-2">Female</FormLabel>
                                </Col>

                            </Row>
                        </Col>
                    </Row>
                </Col>
                <Col sm={6}>
                    <Row className="mt-3">
                        <Col sm={3}>
                            <FormLabel>Passport <span className="text-danger">(*)</span></FormLabel>
                        </Col>
                        <Col sm={9}>
                            <Input></Input>
                        </Col>
                    </Row>
                    <Row className="mt-3">
                        <Col sm={3}>
                            <FormLabel>Email</FormLabel>
                        </Col>
                        <Col sm={9}>
                            <Input></Input>
                        </Col>
                    </Row>
                    <Row className="mt-3">
                        <Col sm={3}>
                            <FormLabel>Country<span className="text-danger">(*)</span></FormLabel>
                        </Col>
                        <Col sm={9}>
                            <Input></Input>
                        </Col>
                    </Row>
                    <Row className="mt-3">
                        <Col sm={3}>
                            <FormLabel>Region</FormLabel>
                        </Col>
                        <Col sm={9}>
                            <Input></Input>
                        </Col>
                    </Row>
                </Col>
            </Row>
            <Row className="mt-3">
                <Row>
                    <Col sm={2}>
                        <FormLabel>Subject</FormLabel>
                    </Col>
                    <Col sm={10}>
                        <TextareaAutosize style={{width:"58rem", height:"10rem"}}></TextareaAutosize>
                    </Col>
                </Row>
            </Row>
            <Row className="mt-3">
                <Col sm={6}>
                </Col>
                <Col sm={6}>
                    <Row className="text-center" style={{marginBottom:"20px"}}>
                        <Col sm={6}> <Button color="success" style={{width:"200px"}}>Send</Button></Col>
                        <Col sm={6}> <Button color="success" style={{width:"200px"}}>Check Data</Button></Col>
                    </Row>
                </Col>
            </Row>

        </Container>
    )
}

export default Reactstrapform