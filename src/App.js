import 'bootstrap/dist/css/bootstrap.css';
import './App.css';
import Reactstrapform from './component/Reactstrapform';

function App() {
  return (
    <div>
      <Reactstrapform></Reactstrapform>
    </div>
  );
}

export default App;
